local mg     = require "moongen"
local memory = require "memory"
local device = require "device"
local ts     = require "timestamping"
local filter = require "filter"
local hist   = require "histogram"
local stats  = require "stats"
local timer  = require "timer"
local arp    = require "proto.arp"
local log    = require "log"
local pcap   = require "pcap"
local ffi	= require "ffi"

-- set addresses here
local DST_MAC		= "01:01:00:00:00:00"
local SCR_MAC		= "01:00:00:00:00:00"
local SRC_IP		= "10.0.0.10"
local DST_IP		= "10.1.0.10"
local SRC_PORT		= 319
local DST_PORT		= 319 --PTP port needed for timestamping


function configure(parser)
	parser:description("Generates UDP traffic and measure latencies. Edit the source to modify constants like IPs.")
	parser:argument("txDev", "Device to transmit from."):convert(tonumber)
	parser:argument("rxDev", "Device to receive from."):convert(tonumber)
	parser:option("-r --rate", "Transmit rate in Mbit/s."):default(10000):convert(tonumber)
	parser:option("-i --intrate", "Transmit rate for int packets in Mbit/s."):default(0):convert(tonumber)
	parser:option("-n --flows", "Number of flows (randomized source IP)."):default(4):convert(tonumber)
	parser:option("-f --file", "Filename of the throughput data."):default("throughput.csv")
	parser:option("-d --fileNtraffic", "Filename of the normal tx traffic data."):default("")
	parser:option("-e --fileItraffic", "Filename of the tx int traffic data."):default("tx-int.csv")
	parser:option("-s --size", "Packet size."):default(62):convert(tonumber)
	parser:option("-l --hist", "Filename of the latency histogram."):default("histogram.csv")
	parser:option("-p --ifile", "Filename of the int metadata."):default("resultINT.csv")
	parser:option("-b --bitmap", "Hexnumber for InstructionIntBitmap (Int Headers)."):default(0xffff)
    parser:option("-x --option", "Option to save INT data"):default(0):convert(tonumber)
end


function master(args)
	txDev = device.config{port = args.txDev, rxQueues = 5, txQueues = 5}
	rxDev = device.config{port = args.rxDev, rxQueues = 5, txQueues = 5, dropEnable = false}
	device.waitForLinks()

	-- max 1kpps timestamping traffic timestamping
	-- rate will be somewhat off for high-latency links at low rates
	if args.rate > 0 then
		txDev:getTxQueue(0):setRate((args.rate - (args.size + 4) * 8 /1000) / 3)
		txDev:getTxQueue(1):setRate((args.rate - (args.size + 4) * 8 /1000) / 3)
		txDev:getTxQueue(2):setRate((args.rate - (args.size + 4) * 8 /1000) / 3)
	end
	if args.intrate > 0 then
		txDev:getTxQueue(3):setRate(args.intrate - (args.size + 4) * 8 /1000)
	end

	--Normal Traffic tasks + stats tx
	for i = 1, 3 do
		mg.startTask("loadSlave", txDev:getTxQueue(i - 1), args.size, args.flows, args.rate, args.fileNtraffic .. "n-tx-D" .. tostring(i-1) .. ".csv", i - 1)
	end

	--Int traffic
	if args.intrate > 0 then
		mg.startTask("loadIntSlave", txDev:getTxQueue(3), rxDev, args.size, args.flows, args.bitmap, args.fileItraffic)
	end


	--Timestamp task
	mg.startTask("timerSlave", txDev:getTxQueue(4), rxDev:getRxQueue(4), args.size, args.flows, args.hist)

	-- Stats Task
	mg.startTask("dumpSlave", rxDev:getRxQueue(0), rxDev, txDev, args.file, args.ifile, args.option)

	mg.waitForTasks()
end

local function fillUdpPacket(buf, len)
	buf:getUdpPacket():fill{
		ethSrc = queue,
		ethDst = DST_MAC,
		ip4Src = SRC_IP,
		ip4Dst = DST_IP,
		udpSrc = SRC_PORT,
		udpDst = DST_PORT,
		pktLength = len
	}
end

local function fillVxlanPacket(buf, len, bitmap)
	buf:getVxlanIntPacket():fill{
		ethSrc = SCR_MAC,
		ethDst = DST_MAC,
		ip4Src = SRC_IP,
		ip4Dst = DST_IP,
		udpSrc = 4790,
		udpDst = 4790,
		vxlanNextProtocol = 0x08,
		inbtLength = 2,
		inbtInstructionBitmap = bitmap,
		inbtRemainingHopCount = 0xff,
		inbtReserved2 = 0xdddd,
		pktLength = len
	}
end

function loadSlave(queue, size, flows, rate, file, device)
	local mempool = memory.createMemPool(function(buf)
		fillUdpPacket(buf, size)
	end)
	local bufs = mempool:bufArray()
	local counter = 0
	local ctr = stats:newPktTxCounter("Normal-Traffic" .. "-Device" .. device, "CSV", file)
	local baseIP = parseIPAddress(SRC_IP)
	while mg.running() do
		bufs:alloc(size)
		for i, buf in ipairs(bufs) do
			local pkt = buf:getUdpPacket()
			pkt.ip4.src:set(baseIP + counter)
			counter = incAndWrap(counter, flows)
			ctr:countPacket(buf)
		end
		-- UDP checksums are optional, so using just IPv4 checksums would be sufficient here
		if rate == 0 then
			-- do nothing
		else
			bufs:offloadUdpChecksums()
			queue:send(bufs)
			ctr:update()
		end
	end
	ctr:finalize()
end

function loadIntSlave(queue, rxDev, size, flows, bitmap, file)
	local mempool = memory.createMemPool(function(buf)
		fillVxlanPacket(buf, size, bitmap)
	end)
	local bufs = mempool:bufArray()
	local counter = 0
	local ctr = stats:newPktTxCounter("Int-Traffic", "CSV", file)
	local baseIP = parseIPAddress(SRC_IP)
	while mg.running() do
		bufs:alloc(size)
		for i, buf in ipairs(bufs) do
			local pkt = buf:getVxlanIntPacket()
			pkt.ip4.src:set(baseIP + counter)
			counter = incAndWrap(counter, flows)
			ctr:countPacket(buf)
		end
		-- UDP checksums are optional, so using just IPv4 checksums would be sufficient here
		bufs:offloadUdpChecksums()
		queue:send(bufs)
		ctr:update()
	end
	ctr:finalize()
end

----------------------
---Timestamping
----------------------

function timerSlave(txQueue, rxQueue, size, flows, file)
	if size < 84 then
		log:warn("Packet size %d is smaller than minimum timestamp size 84. Timestamped packets will be larger than load packets.", size)
		size = 84
	end
	local timestamper = ts:newUdpTimestamper(txQueue, rxQueue)
	local hist = hist:new()
	mg.sleepMillis(1000) -- ensure that the load task is running
	local counter = 0
	local rateLimit = timer:new(0.001)
	local baseIP = parseIPAddress(SRC_IP)
	while mg.running() do
		hist:update(timestamper:measureLatency(size, function(buf)
			fillUdpPacket(buf, size)
			local pkt = buf:getUdpPacket()
			pkt.ip4.src:set(baseIP + counter)
			counter = incAndWrap(counter, flows)
		end))
		rateLimit:wait()
		rateLimit:reset()
	end
	-- print the latency stats after all the other stuff
	mg.sleepMillis(300)
	hist:print()
	hist:save(file)
end

------------------------------
--Stats Receive traffic
------------------------------

local function newIntData()
	--Specify data structure for int meta data
	return {
		PktNumber = {},
		ipAddressSrc = {}, -- 32 bit
		ipAddressDst = {}, -- 32 bit
		switchId = {}, -- 32 bit
		levelOnePortIngress = {}, -- 16 bit
		levelOnePortEgress = {}, --16 bit
		hopLatency = {}, -- 32 bit
		queueId = {}, -- 8 bit
		queueOccupancy = {}, -- 24 bit
		ingressTimestamp = {},--32 bit
		egressTimestamp = {}, -- 32 bit
		levelTwoPortIngress = {}, --32 bit
		levelTwoPortEgress = {}, -- 32bit
		EgressPortUtil = {}, -- 32 bit
	}
end


function writeINTtoCSV(data, ifile)
	-- open output file
	log:info("Saving INT data")
	file = io.open(ifile, "w+")
	-- write below results to file
	--   IpSrcAddress
	--   IpDstAddress
	--   SwitchId
	--   LevelOnePortIngress
	--   LevelOnePortEgress
	--   HopLatency
	--   QueueId
	--   QueueOccupancy
	--   IngressTimestamp
	--   EgressTimestamp
	--   LevelTwoPortIngress
	--   LevelTwoPortEgress
	--   EgressPortTxUtilization
	file:write('PaketNumber' .. "," ..'IpSrcAddress' .. "," .. 'IpDstAddress' .. "," .. 'SwitchId' .. "," .. 'LevelOnePortIngress'
			.. "," .. 'LevelOnePortEgress' .. "," .. 'HopLatency' .. "," .. 'QueueId' .. "," .. 'QueueOccupancy'
			.. "," .. 'IngressTimestamp' .. "," .. 'EgressTimestamp' .. "," .. 'LevelTwoPortIngress'
			.. "," .. 'LevelTwoPortEgress' .. "," .. 'EgressPortTxUtilization'
	)
	file:write('\n')
	for i = 1, table.getn(data.PktNumber), 1 do
		--TODO Format!!!!
		file:write(string.format("%f,%s,%s,%s,%s,%s,%f,%s,%s,%s,%s,%s,%s,%s\n", data.PktNumber[i],data.ipAddressSrc[i], data.ipAddressDst[i], data.switchId[i], data.levelOnePortIngress[i], data.levelOnePortEgress[i], data.hopLatency[i], data.queueId[i], data.queueOccupancy[i], data.ingressTimestamp[i], data.egressTimestamp[i], data.levelTwoPortIngress[i], data.levelTwoPortEgress[i], data.EgressPortUtil[i]))
	end
	file:close()
end

-- Helper function to return padded, unsigned 32bit hex string.
function tohex(x)
	return bit.tohex(x, 2)
end

function tohex8(x)
	return bit.tohex(x, 8)
end

function formatAsHex(data)
	return string.format("%s%s%s%s",
		tohex(bit.band(data, 0xff)), tohex(bit.band(bit.rshift(data, 8 )), 0xff), tohex(bit.band(bit.rshift(data, 16), 0xff)), tohex(bit.band(bit.rshift(data, 24), 0xff)))
end

function formatAsNumber(data)
	return tonumber(string.format("%s%s%s%s",
		tohex(bit.band(data, 0xff)), tohex(bit.band(bit.rshift(data, 8 )), 0xff), tohex(bit.band(bit.rshift(data, 16), 0xff)), tohex(bit.band(bit.rshift(data, 24), 0xff))), 16)
end


--TODO write received int timestamp in histogram ???
function dumpSlave(queue, rxDev, txDev, file, ifile, option)
	local bufs = memory.bufArray()
	local txCtr = stats:newDevTxCounter(txDev, "CSV", file )
	local rxCtr = stats:newDevRxCounter(rxDev, "CSV", file)
	local ctr = stats:newPktRxCounter("Normal Traffic", "CSV", file)
	local ctrInt = stats:newPktRxCounter("Int Traffic", "CSV", file)
	local asVxlanIntPacketX = createStack("eth", "ip4", "udp", { "vxlan", subType = "gpe" }, { "inbt", length = 8 })
	local c = 1
	local intData = newIntData()
	while mg.running() do
		local batchTime = mg.getTime()
		local rx = queue:tryRecv(bufs, 1000)
		for i = 1, rx do
			local buf = bufs[i]
			local pkt = buf:getUdpPacket()
			local port = pkt.udp:getDstPort()
			if port == 319 then
				ctr:countPacket(buf)
			elseif port == 4790 then
				ctrInt:countPacket(buf)
                if option == 1 then 
                    local pkt = asVxlanIntPacketX(buf)
                    meta = pkt.inbt:getMetadata() -- function cost to much processing power

                    --traferse instruction bitmap??? Or allways all int data??
                    bitmap = pkt.inbt:getInstructionBitmap()
                    metaLength = pkt.inbt:getVariableLength()

                    -------------------------------------------------------
                    --log:info("Switch Id:" )
                    --log:info(tostring(formatAsHex(meta[0])))
                    --log:info("Level 1 Port Ingress: " )
                    --log:info(tostring(string.sub(formatAsHex(meta[1]), 1, 4)))
                    --log:info("Level 1 Port Egress:" )
                    --log:info(tostring(formatAsHex(meta[1])))
                    --log:info("Hop:" )
                    --log:info(tostring(formatAsHex(meta[2])))
                    --log:info(tostring(formatAsNumber(meta[2])))
                    --log:info("Queue Id + auslastung" )
                    --log:info(tostring(formatAsHex(meta[3])))
                    --log:info(tostring(formatAsNumber(meta[3])))
                    --log:info("Queue Timestamp Ingress" )
                    --log:info(tostring(formatAsHex(meta[4])))
                    --log:info("Queue Timestamp Egress" )
                    --log:info(tostring(formatAsHex(meta[5])))

                    --extract meta data
                    --for n = 0, metaLength do
                    --    metaData = meta[n]
                    intData.PktNumber[c] = c
                    intData.ipAddressSrc[c] = pkt.ip4:getSrcString() -- 32 bit
                    intData.ipAddressDst[c] = pkt.ip4:getDstString()-- 32 bit
                    intData.switchId[c] = formatAsHex(meta[0]) -- 32 bit
                    intData.levelOnePortIngress[c] = string.sub(formatAsHex(meta[1]), 1, 4) -- 16 bit
                    intData.levelOnePortEgress[c] = string.sub(formatAsHex(meta[1]), 5, 8) --16 bit
                    intData.hopLatency[c] = formatAsNumber(meta[2]) -- 32 bit
                    intData.queueId[c] = string.sub(formatAsHex(meta[3]), 1, 2)  -- 8 bit string
                    intData.queueOccupancy[c] =  string.sub(formatAsHex(meta[3]), 3 ,8)  -- 24 bit
                    intData.ingressTimestamp[c] = formatAsHex(meta[4]) --32 bit
                    intData.egressTimestamp[c] = formatAsHex(meta[5]) -- 32 bit
                    intData.levelTwoPortIngress[c] = formatAsHex(meta[6]) --32 bit
                    intData.levelTwoPortEgress[c] =  formatAsHex(meta[7])-- 32bit
                    intData.EgressPortUtil[c] = formatAsHex(meta[8])
                    c = c + 1
                    end 
			end
		end
		bufs:free(rx)
		txCtr:update()
		rxCtr:update()
		ctr:update()
		ctrInt:update()
	end
	txCtr:finalize()
	rxCtr:finalize()
	ctr:finalize()
	ctrInt:finalize()
	mg.sleepMillis(200)
	if option == 1 then 
        writeINTtoCSV(intData, ifile)
    end
end
