/*
 * INT Metadata Header
 *
 *
 */



header_type ethernet_t {
    fields {
        dstAddr     : 48;
        srcAddr     : 48;
        etherType   : 16;
    }
}

header_type ipv4_t {
    fields {
        version         : 4;
        ihl             : 4;
        diffserv        : 8;
        totalLen        : 16;
        identification  : 16;
        flags           : 3;
        fragOffset      : 13;
        ttl             : 8;
        protocol        : 8;
        hdrChecksum     : 16;
        srcAddr         : 32;
        dstAddr         : 32;
    }
}

header_type udp_t {
    fields {
        srcPort     : 16;
        dstPort     : 16;
        length_     : 16;
        checksum    : 16;
    }
}

header_type vxlan_gpe_t {
    fields {
        flags       : 8;
        reserved    : 16;
        next_proto  : 8;
        vni         : 24;
        reserved2   : 8;
    }
}

//////////////////////////////Metadata-Header
header_type meta_t {
    fields {
        remain_hop_cnt   : 16;
        egress_ts        : 32;
        hop_latency      : 32;
        int_data_length  : 16;
        remain_int_fields : 8;
    }
}

header_type intrinsic_metadata_t {
    fields {
        ingress_global_tstamp : 32;
    }
}

//////////////////////////VXLAN INT header with next_proto field - shim-header
header_type vxlan_gpe_int_header_t {
    fields {
        int_type    : 8;
        reserved    : 8;
        len         : 8;
        next_proto  : 8;
    }
}

// INT header
header_type int_header_t {
    fields {
        ver                     : 4; // Hop-by-Hop(1) or Destination
        replicate               : 2; // Allows for packet duplication
        copy                    : 1; // If packet is a replicated packet set to 1 (identified at the sink)
        e                       : 1; // Max hop count exceeded
        m                       : 1; // MTU exceeded
        reserved1               : 10;// Reserved bits
        hop_ml                  : 5; // Per-hop metadata-length inserted at each hop
        remain_hop_cnt          : 8; // Remaining number of hopy
        instruction_mask_0003   : 4; // split the bits for lookup
        instruction_mask_0407   : 4;
        instruction_mask_0811   : 4;
        instruction_mask_1215   : 4;
        reserved2               : 16; 
        

    }
    
}

// Metadatafield for hop-by-hop INT data
header_type int_metadata_t {
    fields{
        data                    : 32;
    }

}

header_type int_switch_id_header_t {
    fields {
        switch_id           : 32;
    }
}

//Level1 
header_type int_ingress_egress_port_id_header_t {
    fields {
        ingress_port_id    : 16;
        egress_port_id     : 16;
    }
}

//Level2 Virtual Port (4 bytes each)
header_type int_ingress_egress_port_l2_id_header1_t {
    fields {
        ingress_port_id    : 32;
    }
}


header_type int_ingress_egress_port_l2_id_header2_t {
    fields {
        egress_port_id     : 32;
    }
}

header_type int_hop_latency_header_t {
    fields {
        hop_latency         : 32;
    }
}

// Queue id + Queue occupany //TODO
header_type int_q_occupancy_header_t {
     fields {
         q_id                : 8;
         q_occupancy         : 24;
     }
 }

 header_type int_ingress_tstamp_header_t {
     fields {
         ingress_tstamp      : 32;
     }
 }

 header_type int_egress_tstamp_header_t {
     fields {
         egress_tstamp      : 32;
     }
 }


header_type int_egress_port_tx_utilization_header_t {
    fields {
        egress_port_tx_utilization  : 32;
    }
}

//////////////////////////////////////
metadata meta_t meta;
metadata intrinsic_metadata_t intrinsic_metadata;

parser start {
    return parse_ethernet;
}

/*
 * Ethernet
 */


#define ETHERTYPE_IPV4 0x0800

header ethernet_t ethernet;

parser parse_ethernet {
    extract(ethernet);
    return select(latest.etherType) {
        ETHERTYPE_IPV4 : parse_ipv4;
        default : ingress;
    }
}

/*
 * IPv4
 */

#define IP_PROTOCOLS_IPHL_UDP          0x511

header ipv4_t ipv4;

field_list ipv4_checksum_list {
        ipv4.version;
        ipv4.ihl;
        ipv4.diffserv;
        ipv4.totalLen;
        ipv4.identification;
        ipv4.flags;
        ipv4.fragOffset;
        ipv4.ttl;
        ipv4.protocol;
        ipv4.srcAddr;
        ipv4.dstAddr;
}

field_list_calculation ipv4_checksum {
    input {
        ipv4_checksum_list;
    }
    algorithm : csum16;
    output_width : 16;
}

calculated_field ipv4.hdrChecksum  {
    verify ipv4_checksum if (ipv4.ihl == 5);
    update ipv4_checksum if (ipv4.ihl == 5);
}

parser parse_ipv4 {
    extract(ipv4);
    return select(latest.fragOffset, latest.ihl, latest.protocol) {
        IP_PROTOCOLS_IPHL_UDP : parse_udp;
        default: ingress;
    }
}

/*
 * UDP
 */

#define UDP_PORT_VXLAN_GPE             4790//319

header udp_t udp;

parser parse_udp {
    extract(udp);

    return select(latest.dstPort) {
        // Can only parse VXLAN if exist
        UDP_PORT_VXLAN_GPE : parse_vxlan_gpe;
        default: ingress;
    }

}
////////////////////////////////////////////////////////////
/*
 * INT
 */

#define VXLAN_GPE_NEXT_PROTO_INT        0x08 //Value from Specification

header vxlan_gpe_t vxlan_gpe;

parser parse_vxlan_gpe {
    extract(vxlan_gpe);
    return select(vxlan_gpe.next_proto) {
        VXLAN_GPE_NEXT_PROTO_INT : parse_gpe_int_header;
        default : ingress;
    }
}

#define MAX_INT_DATA    16 // variable value - netronome card allows only 16 

header int_header_t                             int_header;
header int_metadata_t                           int_metadata[MAX_INT_DATA];
header int_switch_id_header_t                   int_switch_id_header;
header int_ingress_egress_port_id_header_t      int_ingress_egress_port_id_header;
header int_ingress_egress_port_l2_id_header1_t  int_ingress_egress_port_l2_id_header1;
header int_ingress_egress_port_l2_id_header2_t  int_ingress_egress_port_l2_id_header2;
header int_hop_latency_header_t                 int_hop_latency_header;
header int_q_occupancy_header_t                 int_q_occupancy_header;
header int_ingress_tstamp_header_t              int_ingress_tstamp_header;
header int_egress_tstamp_header_t               int_egress_tstamp_header;
// LEVEL 2 Ingress Port ID
header int_egress_port_tx_utilization_header_t  int_egress_port_tx_utilization_header;
header vxlan_gpe_int_header_t                   vxlan_gpe_int_header;



parser parse_gpe_int_header {
    // GPE uses a shim header to preserve the next_protocol field
    extract(vxlan_gpe_int_header);
    set_metadata(meta.remain_int_fields, vxlan_gpe_int_header.len - 1);
    return parse_int_header;
}


parser parse_int_header
{
    extract(int_header);
    set_metadata(meta.remain_hop_cnt, int_header.remain_hop_cnt);
    return select(meta.remain_int_fields)
    {          
        0x1:       ingress;
        
    default: parse_int_metadata;
    }
}


parser parse_int_metadata {
    extract(int_metadata[next]);
    set_metadata(meta.remain_int_fields, meta.remain_int_fields - 1);
    return select(meta.remain_int_fields) {
        0x1     : ingress;
        //dont ever transition to this state - we don't want to parse any int metadata - only for deparser state
        0xf     : parse_all_int_meta_values_dummy;
        default : parse_int_metadata;
    }
}


//Why extract all headers
parser parse_all_int_meta_values_dummy {
    // bogus state.. just extract all possible int headers in the
    // correct order to build
    // the correct parse graph for deparser (while adding headers)
    extract(int_switch_id_header);
    extract(int_ingress_egress_port_id_header);
    extract(int_hop_latency_header);
    extract(int_q_occupancy_header);
    extract(int_ingress_tstamp_header);
    extract(int_egress_tstamp_header);
    extract(int_ingress_egress_port_l2_id_header1);
    extract(int_ingress_egress_port_l2_id_header2);
    extract(int_egress_port_tx_utilization_header);
    return ingress;
}
///////////////////////////////////////////////////////////////////////////////////////////////////

                                                     //C Sandbox function
primitive_action set_egress_ts();

/////////////////////////////////////////
action do_forward_int(espec)
{
    modify_field(standard_metadata.egress_spec, espec);
}

action do_forward(espec)
{
    modify_field(standard_metadata.egress_spec, espec);
}

action do_drop() {
    drop();
}

table tbl_not_int_forward {
    actions {
        do_forward;
        do_drop;
    }
}
table tbl_forward_int {
    actions {
        do_forward_int;
    }
}

control ingress
{
    if (not valid(int_header)) {
        apply(tbl_not_int_forward);
    } else {
        apply(tbl_forward_int);
    }
}

/*
 * Egress INT processing
 */
/////////////////////////////////////
/* Instr Bit 0 */                                                                    //switch_id
action int_set_header_0() {
    add_header(int_switch_id_header);
    modify_field(int_switch_id_header.switch_id, 0xcafe); // Fixed switch id
    add_to_field(vxlan_gpe_int_header.len, 1);
}

/* Instr Bit 1 */                                                                   //ingress_port_id
action int_set_header_1() {
    add_header(int_ingress_egress_port_id_header);
    modify_field(int_ingress_egress_port_id_header.ingress_port_id, standard_metadata.ingress_port);
    modify_field(int_ingress_egress_port_id_header.egress_port_id,                                       // Egress PORT
        standard_metadata.egress_port);
    add_to_field(vxlan_gpe_int_header.len, 1);
}

/* Instr Bit 2 */                                               //hop_latency
action int_set_header_2() {
    // Value is set in egress controll
    add_header(int_hop_latency_header);
    add_to_field(vxlan_gpe_int_header.len, 1);
}

/* Instr Bit 3 */                                                             //q_id + q_occupancy
action int_set_header_3() {
    //Incomming packets - outgoing packets
    add_header(int_q_occupancy_header);
    modify_field(int_q_occupancy_header.q_id, 0x7f);                            //???
    modify_field(int_q_occupancy_header.q_occupancy, 0xffffff);                 //???
    add_to_field(vxlan_gpe_int_header.len, 1);
}


/* Instr Bit 4 */                                                               //ingress_tstamp
action int_set_header_4() {
    add_header(int_ingress_tstamp_header);
    modify_field(int_ingress_tstamp_header.ingress_tstamp,
                 intrinsic_metadata.ingress_global_tstamp);
    add_to_field(vxlan_gpe_int_header.len, 1);
}
                                                                                //egress_tstamp
/* Instr Bit 5 */
action int_set_header_5() {
    // Timestamp will be set in Egress Control
    add_header(int_egress_tstamp_header);
    add_to_field(vxlan_gpe_int_header.len, 1);
}

/* Instr Bit 6 */
action int_set_header_6() {//Level2 Ingress Port ID's - set dummy vvalues as we don't need it for current tests
    add_header(int_ingress_egress_port_l2_id_header1);
    add_header(int_ingress_egress_port_l2_id_header2);
    modify_field(int_ingress_egress_port_l2_id_header1.ingress_port_id, 0x7FFFFFFF);
    modify_field(int_ingress_egress_port_l2_id_header2.egress_port_id, 0x7FFFFFFF);
    add_to_field(vxlan_gpe_int_header.len, 2);
}

/* Instr Bit 7 */                                                               //egress_port_tx_utilization
action int_set_header_7() {
    add_header(int_egress_port_tx_utilization_header);
    modify_field(int_egress_port_tx_utilization_header.egress_port_tx_utilization, 0x7FFFFFFF); //Dummy value
    add_to_field(vxlan_gpe_int_header.len, 1);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////


table int_inst_0 {
    actions {
        int_set_header_0;
    }
}

table int_inst_1 {
    actions {
        int_set_header_1;
    }
}

table int_inst_2 {
    actions {
        int_set_header_2;
    }
}

table int_inst_3 {
    actions {
        int_set_header_3;
    }
}

table int_inst_4 {
    actions {
        int_set_header_4;
    }
}

table int_inst_5 {
    actions {
        int_set_header_5;
    }
}

table int_inst_6 {
    actions {
        int_set_header_6;
    }
}

table int_inst_7 {
    actions {
        int_set_header_7;
    }
}

///////////////////////////////////////////////////////

action int_update_ipv4() {
    modify_field(meta.int_data_length, int_header.hop_ml * 4);
    add_to_field(ipv4.totalLen, meta.int_data_length); //(4 * int_header.hop_ml)
}

table ip4_length_update {
    actions {
        int_update_ipv4;
    }
}

action set_egress_tstamp_meta_data() {
    set_egress_ts(); //sets meta.egress_ts
}

action set_egress_tstamp() {
    modify_field(int_egress_tstamp_header.egress_tstamp, meta.egress_ts);
}

// Table egress timestamp
table int_egress_tstamp {
    actions {
        set_egress_tstamp;
    }
}

table int_egress_tstamp_meta_data {
    actions {
        set_egress_tstamp_meta_data;
    }

}
////////////
action set_hop_latency() {
    modify_field(meta.hop_latency, meta.egress_ts);
    subtract_from_field(meta.hop_latency, intrinsic_metadata.ingress_global_tstamp);
    modify_field(int_hop_latency_header.hop_latency, meta.hop_latency);
}

// table hop_latency
table int_hop_latency {
    actions {
        set_hop_latency;
    }
}

//Egress actions
action int_hop_cnt_decrement() {
    subtract_from_field(int_header.remain_hop_cnt, 1);
}

table int_hop_cnt_decrement_t {
    actions {
        int_hop_cnt_decrement;
    }
}

action int_hop_cnt_exceeded() {
    modify_field(int_header.e, 1);
}

table int_hop_cnt_exceeded_t {
    actions {
        int_hop_cnt_exceeded;
    }
}

action int_mtu_limit_exceeded() {
    modify_field(int_header.m, 1);
}

table int_mtu_limit_exceeded_t {
    actions {
        int_mtu_limit_exceeded;
    }
}

#define MAX_MTU        1500

control egress
{ // Check which fields are marked as "to add" and if remaining hop count is not zero
    // check hop count exceeded - Decrement INT Hop count
    // INT MTU Limit

    // Check for valid int_header else do nothing
    if (valid(int_header)) {
        // check MTU limit
        if ((standard_metadata.packet_length + 4 * int_header.hop_ml) > MAX_MTU) {
           apply(int_mtu_limit_exceeded_t);
        } else {
           //Check remain hop count
          if (int_header.remain_hop_cnt != 0) {
              apply(int_hop_cnt_decrement_t);
              //Update the ipv4 total length field
              apply(ip4_length_update);
                //ADD IP total length data 
                if ((int_header.instruction_mask_0003 & 0x8) != 0)
                    apply(int_inst_0); //switch id
                if ((int_header.instruction_mask_0003 & 0x4) != 0)
                    apply(int_inst_1); // ports
                if ((int_header.instruction_mask_0003 & 0x2) != 0)
                     apply(int_inst_2);  // hop latency only header without value
                if ((int_header.instruction_mask_0003 & 0x1) != 0)
                    apply(int_inst_3);  // q_occupancy q_id
                if ((int_header.instruction_mask_0407 & 0x8) != 0)
                    apply(int_inst_4); // ingress timestamp
                if ((int_header.instruction_mask_0407 & 0x4) != 0)
                    apply(int_inst_5); //egress timestamp
                if ((int_header.instruction_mask_0407 & 0x2) != 0)
                    apply(int_inst_6); // Virtual Ports if used
                if ((int_header.instruction_mask_0407 & 0x1) != 0)
                    apply(int_inst_7); // egress_port_tx_utilization
            }
        }

        //SET meta data egress timestamp
        apply(int_egress_tstamp_meta_data);

        //SET egress_timestamp value if marked
        if (valid(int_egress_tstamp_header)) {
           apply(int_egress_tstamp);
        }
        // SET hop_latency value if marked
        if (valid(int_hop_latency_header)) {
            apply(int_hop_latency);
        }
        //Set hop count exceeded if marked
        if (int_header.remain_hop_cnt == 0){
            apply(int_hop_cnt_exceeded_t);
        }
    }
}
