
#include <stdint.h>
#include <nfp/me.h>
#include <nfp/mem_atomic.h>
#include <pif_common.h>
#include "pif_plugin.h"


// Set Egress_timestamp
int pif_plugin_set_egress_ts(EXTRACTED_HEADERS_T *headers,
                          MATCH_DATA_T *match_data)
{
    uint64_t ctime;

    ctime = me_tsc_read();

    pif_plugin_meta_set__meta__egress_ts(headers, ctime & 0xffffffff);

    return PIF_PLUGIN_RETURN_FORWARD;
}